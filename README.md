Go Aries! project summary
===================

The "Go Aries!" project focuses on enabling Anoncreds by supporting CL-signatures within the Aries Framework Go. This approach builds a cryptography-based “bridge” to make the Aries Framework Go compatible with SSI agents and blockchains that implement Anoncreds. This allows for combining the best of both (and beyond) worlds and could be the foundation for a new way of SSI interoperability.

## Background and Goals
Hyperledger Aries is the dominant protocol to enable SSI applications and the interaction between agents. The most popular framework is the ACA-Py which enables cloud-based SSI agents written in Python and is closely entangled with Hyperledger Indy, a purpose-built permissioned blockchain as a trust anchor. The dominant signature scheme is the Camenisch-Lysyanskaya (CL) signature scheme to sign and verify DIDs and Anoncreds, another implementation of credentials besides the verifiable credential standard by the W3C.

The Aries Framework Go (AfGo) comes more from a ledger-independent approach, which is based on Golang and natively supports verifiable credentials with support for various cryptography schemes like BLS keys for privacy-preserving BBS+ signatures. Due to its language wrappers, it can be deployed directly on machines which gives it a crucial advantage over the ACA-Py and its corresponding mobile frameworks. 
To make machine interaction available to ACA-Py Agents, AfGo agents need to support CL signatures. We want to add support for these signatures to the Aries Framework Go to make it more complete and allow for new use cases in the SSI ecosystem.

For a more detailed explanation of Anoncreds, read this [blogpost](https://docs.cheqd.io/identity/building-decentralized-identity-apps/verifiable-credentials/anoncreds). The W3C Verifiable Credential standard can be found [here](https://www.w3.org/TR/vc-data-model/).  

## Problem Statement
There are currently two dominant universes within the Hyperledger Aries Framework. The ACA-Py in connection with Hyperledger Indy is already a mature agent- and trust anchor framework and is the most used framework for SSI projects to date. However, it has its limitations as is aimed to be a cloud agent and does not support mobile or other kinds of edge agents. Even though the compatible Aries Framework .NET or -Javascript provides functionalities for mobile devices, it cannot be implemented on standalone devices like car-internal computers. This limits the use of the ACA-Py primarily on cloud agents and mobile devices. 

The Aries Framework Go comes from a more independent approach and is not tied to a specific ledger. It can be connected to Hyperledger Indy but comes with a different form of signature schemes which limits today's interoperability between these universes. In order to have strong interfaces between these frameworks, the Aries Framework Go requires support for Anoncreds. 

## Solution approach
Our project uses the Aries Framework Go as the starting point as this approach ensures high compatibility with existing projects based on the Aries Framework Go and it allows us to easily provide merge requests. We are rewriting existing components to include Anoncreds that are required for a well-performing Aries Framework Go agent. We will also focus on the shared rust libraries and especially Indy CredX for credential handling. Therefore, our new Anoncreds-compatible Go agents would be able to create connections by establishing communication channels via DIDcomm, anchor public DIDs on Indy, issue, store and verify Anoncreds, and create and read from revocation registries. Furthermore, this makes the Anoncreds-supporting frameworks also independent from Hyperledger Indy, as it is possible to connect any ledger as a Verifiable Data Registry. This is a major leap towards completing the SSI framework as it unifies the two credential schemes and supporting blockchains.

## Results
As our first step, we implemented a go-wrapper for the indy-credx library. This allows for calling the indy-credx functions for credential management from a Golang environment, such as a vehicle or a mobile device. Our [pull request](https://github.com/hyperledger/indy-shared-rs/pull/15) was successfully merged in the [shared rust](https://github.com/hyperledger/indy-shared-rs) libraries and is openly accessible for others.

In the next step, we implemented an agent with the Aries Framework Go that support Anoncreds. To do so, we created a simple VDR that represents any blockchain with Anoncred support, like Hyperledger Indy or cheqd. Here, we published a credential definition for the Anoncred as well as a revocation registry with cryptographic accumulators along with a shared volume that contains the tails file where issuers can create the proof of non-revocation. The simple VDR and the tails file are accessible for the holder and verifier as well to verify the Anoncred.

After the Anoncred with its signatures and proofs has been created by the issuer, it will be wrapped by a W3C verifiable credential so that the Anoncred itself is one attribute. It is now ready to be sent to a corresponding agent and can be requested to be presented by a verifier. In this case, the verifier asks for the one attribute that contains the Anoncred which will be shared by the holder. The corresponding code can be found in our eSSIF-repository.

In the process of developing the Aries Bridge, we also updated the indy-vdr component to be compatible with the Aries Framework Go v0.1.6. The [pull request](https://github.com/hyperledger/aries-framework-go-ext/pull/160) has already been merged as well. 

## Similar Community Efforts
Similar efforts are undertaken by others as well. For instance there are [efforts](https://github.com/hyperledger/aries-framework-go/pull/3271) that aim to enable the same functionalities by modifying the underlying cryptography in Hyperledger Ursa, or esatus provided a [wrapper](https://github.com/hyperledger/indy-shared-rs/pull/17) for the .NET framework. 
